                        ****JAVA PROGRAMMING FOR BEGINNERS****
**Introduction to Java	**
                   
                        [Java](https://www.credosystemz.com/java-training/) is defined by a specification and consists of a programming language, a compiler, core libraries and a runtime (Java virtual machine) The Java runtime allows software developers to write program code in other languages than the [Java programming language](https://www.credosystemz.com/java-training/) which still runs on the Java virtual machine. The Java platform is usually associated with the Java virtual machine and the Java core libraries.
												![](https://www.credosystemz.com/java-training/)
 **Properties of Java**
•	Platform independent
•	Object-orientated programming language
•	Strongly-typed programming language
•	Interpreted and compiled language
•	Automatic memory management


**Java virtual machine**
        The [Java virtual machine](https://www.credosystemz.com/java-training/) (JVM) is a software implementation of a computer that executes programs like a real machine.
The Java virtual machine is written specifically for a specific operating system, e.g., for Linux a special implementation is required as well as for Windows.
                             
**Classes and Objects**
                                        
                Classes and Objects are basic concepts of Object Oriented Programming which revolve around the real life entities.
**Classes in Java**
         The class is a group of similar entities. It is only an logical component and not the physical entity.
                             
A class can contain any of the following variable types.
Local variables − Variables defined inside methods, constructors or blocks are called local variables. The variable will be declared and initialized within the method and the variable will be destroyed when the method has completed.
Instance variables − Instance variables are variables within a class but outside any method. These variables are initialized when the class is instantiated. Instance variables can be accessed from inside any method, constructor or blocks of that particular class.
Class variables − Class variables are variables declared within a class, outside any method, with the static keyword.

 **Object in Java**
                  An Object contains both the data and the function, which operates on the data. An entity that has state and behaviour is known as an object e.g., chair, bike, marker, pen, table, car, etc. It can be physical or logical (tangible and intangible). 
               
The example of an intangible object is the banking system. An object has three characteristics:
State: represents the data (value) of an object.
Behaviour: represents the behaviour (functionality) of an object such as deposit, withdraw, etc.
Identity: An object identity is typically implemented via a unique ID. The value of the ID is not visible to the external user. However, it is used internally by the JVM to identify each object uniquely.
Example - chair, bike, marker, pen, table, car, etc.

**Classification of OOPS**
•	Unstructured Programming Languages
•	Structured Programming Languages
•	Object Oriented Programming
Unstructured Programming Languages
LET S = 0  
MAT INPUT V 
LET N = NUM 
IF N = 0 THEN 99 
FOR I = 1 TO N 
LET S = S + V(I) 
NEXT I 
PRINT S/N 
GO TO 5 
END

**Structured Programming Languages**
In object oriented programming, program is divided into small parts called objects. ... In object oriented programming, data is more important than function. Procedural programming is based on unreal world. Object oriented programming is based on real world.
**Object Oriented Programming**
[Object-oriented programming ](https://www.credosystemz.com/java-training/)(OOP) refers to a type of computer programming (software design) in which programmers define not only the data type of a data structure, but also the types of operations (functions) that can be applied to the data structure.
Conclusion: for further details about Java and step by step explanation visit. https://www.credosystemz.com/java-training/
